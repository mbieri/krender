@file:Suppress("SameParameterValue")

import io.ImageBuffer
import io.ImageFormat.PPM
import math.Quaternion
import math.Ray
import math.StraightSampler
import math.Vector2d
import math.Vector3d
import math.Vector3d.Companion.BLUE
import math.Vector3d.Companion.GREEN
import math.Vector3d.Companion.ORIGIN
import math.Vector3d.Companion.RED
import math.Vector3d.Companion.WHITE
import math.Vector3d.Companion.Y
import math.Vector3d.Companion.Z
import math.Vector3d.Companion.color
import math.Vector3d.Companion.point
import math.Vector3d.Companion.vector
import math.invoke
import math.minus
import math.plus
import mu.KLogging
import scene.PerspectiveCamera
import scene.Plane
import scene.Scene
import scene.Sphere
import scene.VerticalGradient
import java.io.File
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime
import kotlin.time.measureTimedValue

@OptIn(ExperimentalTime::class)
class KRender {

    companion object : KLogging() {

        @JvmStatic
        fun main(args: Array<String>) {
            if (args.isEmpty()) {
                haveFun()
            } else {

                when (args.first()) {
                    "-emptyScene" -> renderEmptyScene()
                    "-singleSphere" -> renderSingleSphere()
                    "-simpleScene" -> renderSimpleScene()
                    else -> haveFun()
                }

            }

        }

        private fun renderEmptyScene() {

            // setup scene with a gradient background and a single sphere
            val aspectRatio = 16.0 / 9.0
            val imageWidth = 32
            val imageHeight = (imageWidth.toDouble() / aspectRatio).toInt()

            val scene = Scene(
                shapes = listOf(),
                camera = PerspectiveCamera(
                    position = point(.0, .0, -10.0),
                    direction = Z,
                    up = Y,
                    aspectRatio = aspectRatio,
                    focalLength = 5.0
                ),
                sampler = StraightSampler(),
                background = VerticalGradient(
                    topColor = color(.5, .7, 1.0),
                    bottomColor = WHITE
                ),
                image = ImageBuffer(
                    width = imageWidth,
                    height = imageHeight,
                    fallbackColor = GREEN
                ),
            )

            // render scene
            logger.info { "start rendering of $scene" }
            val renderStats = measureTimedValue { scene.render() }
            logger.info { "image rendered in ${renderStats.duration.inWholeMilliseconds}ms" }
            logger.info { "stats: ${renderStats.value}" }

            // write out image file
            val renderedScene = renderStats.value
            val imageFile = File.createTempFile("renderedScene", "PPM")
            logger.info { "writing rendered image to $imageFile" }
            val writeTime = measureTime { renderedScene.renderedImage.writeToImage(imageFile, PPM) }
            logger.info { "${imageFile.length()} bytes written to image file in ${writeTime.inWholeMilliseconds}ms" }
        }

        private fun renderSingleSphere() {

            // setup scene with a gradient background and a single sphere
            val aspectRatio = 16.0 / 9.0
            val imageWidth = 32
            val imageHeight = (imageWidth.toDouble() / aspectRatio).toInt()

            val scene = Scene(
                shapes = listOf(
                    Sphere(
                        center = ORIGIN,
                        radius = 1.0,
                        color = RED,
                    )
                ),
                camera = PerspectiveCamera(
                    position = point(.0, .0, -10.0),
                    direction = Z,
                    up = Y,
                    aspectRatio = aspectRatio,
                    focalLength = 5.0
                ),
                background = VerticalGradient(
                    topColor = color(.5, .7, 1.0),
                    bottomColor = WHITE
                ),
                sampler = StraightSampler(),
                image = ImageBuffer(
                    width = imageWidth,
                    height = imageHeight,
                    fallbackColor = GREEN
                ),
            )

            // render scene
            logger.info { "start rendering of $scene" }
            val renderStats = measureTimedValue { scene.render() }
            logger.info { "image rendered in ${renderStats.duration.inWholeMilliseconds}ms" }
            logger.info { "stats: ${renderStats.value}" }

            // write out image file
            val renderedScene = renderStats.value
            val imageFile = File.createTempFile("renderedScene", "PPM")
            logger.info { "writing rendered image to $imageFile" }
            val writeTime = measureTime { renderedScene.renderedImage.writeToImage(imageFile, PPM) }
            logger.info { "${imageFile.length()} bytes written to image file in ${writeTime.inWholeMilliseconds}ms" }
        }

        private fun renderSimpleScene() {

            // setup scene with a gradient background and a single sphere
            val aspectRatio = 16.0 / 9.0
            val imageWidth = 1280
            val imageHeight = (imageWidth.toDouble() / aspectRatio).toInt()

            val sphereRadius = 1.0

            val scene = Scene(
                shapes = listOf(
                    Sphere(
                        center = ORIGIN,
                        radius = sphereRadius,
                        color = RED,
                    ),
                    Plane(
                        point = ORIGIN + vector(y = -sphereRadius),
                        normal = Y,
                        color = BLUE + GREEN
                    )
                ),
                camera = PerspectiveCamera(
                    position = point(.0, .0, -10.0),
                    direction = Z,
                    up = Y,
                    aspectRatio = aspectRatio,
                    focalLength = 5.0
                ),
                background = VerticalGradient(
                    topColor = color(.5, .7, 1.0),
                    bottomColor = WHITE
                ),
                sampler = StraightSampler(),
                image = ImageBuffer(
                    width = imageWidth,
                    height = imageHeight,
                    fallbackColor = GREEN,
                    minRelevantSamples = 4
                ),
            )

            // render scene
            logger.info { "start rendering of $scene" }
            val renderStats = measureTimedValue { scene.render(maxVariance = .01, maxIteration = 10) }
            logger.info { "image rendered in ${renderStats.duration.inWholeMilliseconds}ms" }
            logger.info { "stats: ${renderStats.value}" }

            // write out image file
            val renderedScene = renderStats.value
            val imageFile = File("simpleScene.ppm")
            logger.info { "writing rendered image to $imageFile" }
            val writeTime = measureTime { renderedScene.renderedImage.writeToImage(imageFile, PPM, 2.2) }
            logger.info { "${imageFile.length()} bytes written to image file in ${writeTime.inWholeMilliseconds}ms" }
        }

        private fun haveFun() {
            logger.info("Hello World!!")
            logger.info("=============")

            val vector2d = Vector2d.vector(
                x = 3.5,
                y = 1.0
            )

            val vector2dNormalized = vector2d.normalized

            val quaternion = Quaternion(
                v = Vector3d(4.0, 3.0, 2.0),
                w = 1.0
            )

            val ray = Ray(origin = ORIGIN, direction = vector(x = 1.0, y = 1.0).normalized)

            logger.info("v = $vector2d")
            logger.info(" ---")

            logger.info { "n = $vector2dNormalized, n.length = ${vector2dNormalized.length}" }
            logger.info(" ---")

            logger.info("q = $quaternion")
            logger.info(" ---")

            logger.info("r = $ray")
            logger.info { "r@10 = ${ray(10.0)}, r@10.length = ${(ray(10.0) - ray.origin).length}" }
            logger.info(" ---")

            // create a color gradient and save the image to a temp-file
            createColorGradient(64)
        }

        private fun createColorGradient(imageWidth: Int) {
            val aspectRatio = 1.0 / 1.0 // 16/9 aspect ratio derived from the full-hd format :-)
            val imageHeight = (imageWidth / aspectRatio).toInt()
            val testImage = ImageBuffer(imageWidth, imageHeight, Vector3d.BLACK)
            val imageCreationDuration = measureTime {
                for (row in 0 until testImage.height) {
                    for (col in 0 until testImage.width) {
                        testImage[col, row] = color(
                            r = col.toDouble() / testImage.width,
                            g = row.toDouble() / testImage.height,
                            b = .25
                        )
                    }
                }
            }
            logger.info { "image created: $testImage in ${imageCreationDuration.inWholeMilliseconds}ms" }

            val imageFormat = PPM
            val imageFile = File.createTempFile("image", imageFormat.name)
            val imageFileCreationDuration = measureTime { testImage.writeToImage(imageFile, imageFormat) }

            logger.info { "wrote image file: $imageFile length=${imageFile.length()} Bytes in ${imageFileCreationDuration.inWholeMilliseconds}ms" }

            if (logger.isDebugEnabled)
                imageFile.useLines { it.take(10).forEach { line -> logger.debug { line } } }

            logger.debug("...")
        }

    }
}
