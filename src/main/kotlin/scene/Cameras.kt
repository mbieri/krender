package scene

import math.P3d
import math.Ray
import math.UV
import math.V3d
import math.cross
import math.mapRange
import math.plus
import math.times

interface Camera {
    fun generateCameraRay(uv: UV): Ray
}

data class PerspectiveCamera(val position: P3d, val direction: V3d, val up: V3d, val aspectRatio: Double, val focalLength: Double) : Camera {

    internal val right = (direction cross up).normalized * aspectRatio

    override fun generateCameraRay(uv: UV): Ray {
        val (u, v) = uv

        assert(u in .0..1.0) { "u ($u) must be in the range of 0.0 and 1.0" }
        assert(v in .0..1.0) { "v ($v) must be in the range of 0.0 and 1.0" }

        val targetPoint = position +
                (direction * focalLength) +
                (right * mapRange(targetStart = -1.0, targetEnd = 1.0, value = u)) +
                (up * mapRange(targetStart = -1.0, targetEnd = 1.0, value = v))

        return Ray.from(position, targetPoint)
    }
}
