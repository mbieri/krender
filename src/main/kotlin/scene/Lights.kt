package scene

import math.Color
import math.P3d

interface Lights {
    val isDeltaLight: Boolean
}

data class PointLight(val position: P3d, val color: Color) : Lights {
    override val isDeltaLight: Boolean
        get() = true
}
