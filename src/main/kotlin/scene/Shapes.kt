package scene

import math.EPSILON
import math.N3d
import math.P3d
import math.Ray
import math.Vector3d
import math.Vector3d.Companion.color
import math.dot
import math.invoke
import math.mapRange
import math.minus
import math.times
import mu.KLogging
import java.util.*
import kotlin.math.abs
import kotlin.math.sqrt

interface Shape {
    fun hit(ray: Ray): CollisionInformation

    fun normalAt(p: P3d): N3d

}

data class Sphere(private val center: P3d, private val radius: Double, private val color: Vector3d) : Shape {
    override fun hit(ray: Ray): CollisionInformation {
        val oc = ray.origin - center
        val a = ray.direction dot ray.direction
        val b = 2.0 * (oc dot ray.direction)
        val c = (oc dot oc) - (radius * radius)

        val solutions = solveQuadratic(a, b, c)

        return when (solutions.size) {
            0 -> NoCollision
            else -> {
                val relevantSolution = solutions.first()
                val collisionPoint = ray(relevantSolution)
                val normal = normalAt(collisionPoint)
                val collisionColor = color(
                    r = mapRange(sourceStart = -1.0, value = normal.x),
                    g = mapRange(sourceStart = -1.0, value = normal.y),
                    b = mapRange(sourceStart = -1.0, value = normal.z)
                ) * .5
                CollisionInformation(
                    distance = relevantSolution,
                    shape = this,
                    point = collisionPoint,
                    normal = normal,
                    color = collisionColor,
                )
            }
        }

    }

    override fun normalAt(p: P3d) = (p - center).normalized

    companion object : KLogging() {

        fun solveQuadratic(a: Double, b: Double, c: Double): SortedSet<Double> {
            val discriminant = b * b - 4 * a * c
            return when {
                discriminant < .0 -> sortedSetOf()
                discriminant == .0 -> sortedSetOf(-b / (2.0 * a))
                else -> listOf(
                    (-b - sqrt(discriminant)) / (2.0 * a),
                    (-b + sqrt(discriminant)) / (2.0 * a)
                )
                    .filter { it >= .0 }
                    .toSortedSet()
            }
        }

    }

}

data class Plane(private val point: P3d, private val normal: N3d, val color: Vector3d) : Shape {
    override fun hit(ray: Ray): CollisionInformation {
        // denominator gets zero if the ray and the plane are parallel -> no collision
        val denominator = (normal dot ray.direction)

        if (abs(denominator) >= EPSILON) {
            val t = ((point - ray.origin) dot normal) / denominator

            // t > 0 means the collision is in the direction of the ray.
            // otherwise the collision is irrelevant
            if (t >= EPSILON) {
                return CollisionInformation(
                    distance = t,
                    shape = this,
                    point = ray(t),
                    normal = normal,
                    color = color
                )
            }
        }

        return NoCollision
    }

    override fun normalAt(p: P3d) = normal

    companion object : KLogging()
}
