package scene

import math.Color
import math.UV
import math.plus
import math.times

interface Background {
    fun backgroundColor(uv: UV): Color
}

class Solid(private val backgroundColor: Color) : Background {
    override fun backgroundColor(uv: UV) = backgroundColor

    override fun toString(): String {
        return "Solid(backgroundColor=$backgroundColor)"
    }
}

class VerticalGradient(private val topColor: Color, private val bottomColor: Color) : Background {
    override fun backgroundColor(uv: UV) = bottomColor * (1.0 - uv.v) + topColor * uv.v

    override fun toString(): String {
        return "VerticalGradient(topColor=$topColor, bottomColor=$bottomColor)"
    }

}
