package scene

import io.ImageBuffer
import math.Color
import math.Ray
import math.Sampler
import math.Vector2d.Companion.uv
import math.Vector3d
import math.Vector3d.Companion.BLACK
import mu.KLogging

class Scene(
    private val shapes: List<Shape>,
    private val camera: Camera,
    private val background: Background,
    private val sampler: Sampler,
    private val image: ImageBuffer,
) {

    private var maxDepth: Int = 0

    fun render(maxVariance: Double = Double.MAX_VALUE, maxIteration: Int = 10, maxRayDepth: Int = 1): RenderedScene {

        logger.info { "start rendering" }

        maxDepth = maxRayDepth
        var renderingIterations = 0

        var rayCount = 0

        val uStep = 1.0 / image.width
        val vStep = 1.0 / image.height
        val halfUStep = uStep / 2.0
        val halfVStep = vStep / 2.0

        var pixelToRender = pixelToRender(maxVariance)

        do {

            renderingIterations++
            logger.info { "starting iteration: $renderingIterations, pixel to render: ${pixelToRender.size}" }

            for ((col, row) in pixelToRender) {
                val uv = sampler.jitter(
                    uv = uv(col * uStep + halfUStep, row * vStep + halfVStep),
                    uSize = uStep,
                    vSize = vStep
                )
                val cameraRay = camera.generateCameraRay(uv)
                val collision = traceRay(
                    ray = cameraRay,
                    currentDepth = 0 // camera rays always start with 0 as depth
                )
                val resultingColor = collision.color ?: background.backgroundColor(uv)
                image[col, row] = resultingColor
                rayCount++
            }

            pixelToRender = pixelToRender(maxVariance)

        } while (pixelToRender.isNotEmpty() && (renderingIterations < maxIteration))

        logger.info { "done rendering after $renderingIterations iterations" }

        return RenderedScene(
            rayCount,
            image,
            ImageBuffer(image.width, image.height, BLACK),
            ImageBuffer(image.width, image.height, BLACK)
        )
    }

    private fun pixelToRender(maxVariance: Double): MutableList<Pair<Int, Int>> {
        val pixelToRender = mutableListOf<Pair<Int, Int>>()
        for (u in 0 until image.width) {
            for (v in 0 until image.height) {
                val sample = image.colorSampleAt(u, v)
                if (sample.variance >= maxVariance) {
                    pixelToRender.add(u to v)
                }
            }
        }
        return pixelToRender
    }

    private fun traceRay(ray: Ray, currentDepth: Int) =
        if (currentDepth >= maxDepth)
            NoCollision
        else
            shapes
            .map { shape -> shape.hit(ray) }
            .filter { it != NoCollision }
            .minByOrNull { it.distance } ?: NoCollision

    override fun toString(): String {
        return "Scene(Shapes=${shapes.size}, Camera=$camera, Sampler=$sampler, Background=$background, Image=$image)"
    }

    companion object : KLogging() {

    }
}

open class CollisionInformation(
    val distance: Double,
    val shape: Shape?,
    val point: Vector3d?,
    val normal: Vector3d?,
    val color: Color?
) {
    override fun toString(): String {
        return "CollisionInformation(distance=$distance, shape=$shape, color=$color)"
    }
}

object NoCollision :
    CollisionInformation(distance = Double.MAX_VALUE, shape = null, point = null, normal = null, color = null)

data class RenderedScene(
    val rayCount: Int,
    val renderedImage: ImageBuffer,
    val depthMap: ImageBuffer,
    val solidMap: ImageBuffer
)

