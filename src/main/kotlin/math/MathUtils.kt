package math

const val EPSILON = 1.0e-6

fun mapRange(sourceStart: Double = .0, sourceEnd: Double = 1.0, targetStart: Double = .0, targetEnd: Double = 1.0, value: Double) =
    targetStart +
            (value - sourceStart) /
            (sourceEnd - sourceStart) *
            (targetEnd - targetStart)
