package math

import kotlin.math.min
import kotlin.math.sqrt

data class Vector3d internal constructor(val x: Double, val y: Double, val z: Double) {

    val r: Double
        inline get() = x

    val g: Double
        inline get() = y

    val b: Double
        inline get() = z

    val lengthSquared: Double
        inline get() = x * x + y * y + z * z

    val length: Double
        inline get() = sqrt(lengthSquared)

    val normalized: Vector3d
        inline get() = this / length

    fun toRGB(maxColorComponentValue: Int = 255) = Triple(
        (r * (maxColorComponentValue + .999)).toInt(),
        (g * (maxColorComponentValue + .999)).toInt(),
        (b * (maxColorComponentValue + .999)).toInt(),
    )

    companion object {
        fun color(r: Double = .0, g: Double = .0, b: Double = .0) = Vector3d(r, g, b)
        fun vector(x: Double = .0, y: Double = .0, z: Double = .0) = Vector3d(x, y, z)
        fun point(x: Double = .0, y: Double = .0, z: Double = .0) = Vector3d(x, y, z)
        fun normal(x: Double = .0, y: Double = .0, z: Double = .0) = Vector3d(x, y, z)

        // origin
        val ORIGIN = point()
        val NULL = vector()

        // unit vectors
        val X = vector(x = 1.0)
        val Y = vector(y = 1.0)
        val Z = vector(z = 1.0)

        // basic colors
        val BLACK = color()
        val WHITE = color(r = 1.0, g = 1.0, b = 1.0)
        val RED = color(r = 1.0)
        val GREEN = color(g = 1.0)
        val BLUE = color(b = 1.0)
    }
}

typealias V3 = Vector3d
typealias V3d = Vector3d
typealias N3d = Vector3d
typealias P3d = Vector3d

typealias Color = Vector3d

operator fun Vector3d.unaryMinus() = Vector3d(-x, -y, -z)

operator fun Vector3d.plus(other: Vector3d) = Vector3d(x + other.x, y + other.y, z + other.z)
operator fun Vector3d.minus(other: Vector3d) = Vector3d(x - other.x, y - other.y, z - other.z)

operator fun Vector3d.times(s: Double) = Vector3d(x * s, y * s, z * s)
operator fun Vector3d.times(i: Int) = this * i.toDouble()

operator fun Vector3d.div(s: Double) = Vector3d(x / s, y / s, z / s)
operator fun Vector3d.div(i: Int) = this / i.toDouble()

infix fun Vector3d.dot(other: Vector3d) = x * other.x + y * other.y + z * other.z
infix fun Vector3d.cross(other: Vector3d) = Vector3d(
    y * other.z - z * other.y,
    z * other.x - x * other.z,
    x * other.y - y * other.x
)

infix fun Vector3d.clamp(limit: Double) = Vector3d(
    x = min(limit, x),
    y = min(limit, y),
    z = min(limit, z)
)
