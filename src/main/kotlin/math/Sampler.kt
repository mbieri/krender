package math

import math.Vector2d.Companion.uv
import kotlin.random.Random

interface Sampler {

    fun jitter(uv: UV, uSize: Double, vSize: Double): UV

}

class StraightSampler() : Sampler {
    override fun jitter(uv: UV, uSize: Double, vSize: Double) = uv

    override fun toString() = "StraightSampler"
}

class RandomSampler() : Sampler {
    override fun jitter(uv: UV, uSize: Double, vSize: Double) =
        uv(
            u = uv.u + Random.nextDouble(-uSize / 2, uSize / 2),
            v = uv.v + Random.nextDouble(-vSize / 2, vSize / 2)
        )

    override fun toString() = "RandomSampler"
}
