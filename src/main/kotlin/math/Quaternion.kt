package math

data class Quaternion internal constructor(val v: Vector3d, val w: Double)

operator fun Quaternion.unaryMinus() = Quaternion(-v, -w)

operator fun Quaternion.plus(q: Quaternion) = Quaternion(v + q.v, w + q.w)
operator fun Quaternion.minus(q: Quaternion) = Quaternion(v - q.v, w - q.w)

operator fun Quaternion.times(s: Double) = Quaternion(v * s, w * s)
operator fun Quaternion.div(s: Double) = Quaternion(v / s, w / s)
