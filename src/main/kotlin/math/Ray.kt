package math

data class Ray internal constructor(val origin: P3d, val direction: V3d) {

    val normalized: Ray
        get() = Ray(origin, direction.normalized)

    companion object {
        fun from(origin: P3d, target: P3d) = Ray(
            origin,
            (target - origin).normalized
        )
    }

}

operator fun Ray.invoke(t: Double) = origin + (direction * t)
