package math

import kotlin.math.sqrt

data class Vector2d internal constructor(val x: Double, val y: Double) {

    val u: Double
        inline get() = x

    val v: Double
        inline get() = y

    val lengthSquared: Double
        inline get() = x * x + y * y

    val length: Double
        inline get() = sqrt(x * x + y * y)

    val normalized: Vector2d
        inline get() = this / length

    companion object {
        fun vector(x: Double = .0, y: Double = .0) = Vector2d(x, y)
        fun uv(u: Double = .0, v: Double = .0) = Vector2d(u, v)

        val X = vector(x = 1.0)
        val Y = vector(y = 1.0)

        val U = uv(u = 1.0)
        val V = uv(v = 1.0)
    }

}

typealias V2 = Vector2d
typealias V2d = Vector2d
typealias P2d = Vector2d
typealias UV = Vector2d

operator fun Vector2d.unaryMinus() = Vector2d(-x, -y)

operator fun Vector2d.plus(other: Vector2d) = Vector2d(x + other.x, y + other.y)
operator fun Vector2d.minus(other: Vector2d) = Vector2d(other.x - x, other.y - y)

operator fun Vector2d.times(s: Double) = Vector2d(x * s, y * s)
operator fun Vector2d.div(s: Double) = Vector2d(x / s, y / s)

infix fun Vector2d.dot(other: Vector2d) = x * other.x + y * other.y
