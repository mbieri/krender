package io

import math.Color
import math.UV
import math.Vector3d.Companion.color
import math.div
import math.mapRange
import math.plus
import mu.KLogging
import java.io.File
import java.io.FileWriter
import kotlin.math.pow

class ImageBuffer(
    val width: Int,
    val height: Int,
    private val fallbackColor: Color,
    private val minRelevantSamples: Int = 2
) {
    private var _data: Array<Color>? = null

    private val rawData = Array(width * height) { ColorSample(fallbackColor = fallbackColor, minRelevantSamples) }

    val data: Array<Color>
        get() {
            synchronized(this) {
                refreshDataCache()
                return _data as Array<Color>
            }
        }

    fun colorSampleAt(col: Int, row: Int) = rawData[toArrayIndex(col, row)]

    fun writeToImage(target: File, format: ImageFormat = ImageFormat.PPM, gamma: Double = 2.0) {
        synchronized(this) {
            when (format) {
                ImageFormat.PPM -> writePPMFile(target, gamma)
                else -> TODO("image format $format not implemented yet")
            }
        }
    }

    /**
     * Write the image buffer to a PPM file.
     * For ppm format info check out http://paulbourke.net/dataformats/ppm/
     */
    private fun writePPMFile(target: File, gamma: Double) {

        FileWriter(target).use {

            refreshDataCache()

            // write header:
            //
            // P3
            // <width> <height>
            // <max value for a color component>
            //

            // change for color resolution, 255 is default
            val maxColorComponentValue = 255

            it.appendLine("P3")
                .appendLine("$width $height")
                .appendLine("$maxColorComponentValue")

            // append pixel colors
            // starting from top left of the image
            for (row in (0 until height).reversed()) {
                for (col in 0 until width) {
                    val (r, g, b) = _data!![toArrayIndex(col, row)].withGammaCorrection(gamma)
                        .toRGB(maxColorComponentValue)
                    it.appendLine("$r $g $b")
                }
            }

            it.flush()
        }
    }

    private fun refreshDataCache() {
        if (_data == null) {
            _data = Array(rawData.size) { index -> rawData[index].color }
        }
    }

    private fun toArrayIndex(col: Int, row: Int) = row * width + col

    private fun toArrayIndex(col: Double, row: Double) =
        toArrayIndex(
            (mapRange(targetEnd = (width - 1.0), value = col)).toInt(),
            (mapRange(targetEnd = (height - 1.0), value = row)).toInt()
        )

    operator fun set(col: Int, row: Int, color: Color) {
        assert(col in 0 until width) { "col must be in the range of 0 and ${width - 1}" }
        assert(row in 0 until height) { "row must be in the range of 0 and ${height - 1}" }

        synchronized(this) {
            rawData[toArrayIndex(col, row)].push(color)
            _data = null
        }
    }

    operator fun set(col: Double, row: Double, color: Color) {
        assert(col in .0..1.0) { "col must be in the range of 0.0 and 1.0" }
        assert(row in .0..1.0) { "row must be in the range of 0.0 and 1.0" }

        synchronized(this) {
            rawData[toArrayIndex(col, row)].push(color)
            _data = null
        }
    }

    operator fun set(uv: UV, color: Color) = set(uv.u, uv.v, color)

    operator fun get(col: Int, row: Int): Color {
        assert(col in 0 until width) { "col must be in the range of 0 and ${width - 1}" }
        assert(row in 0 until height) { "row must be in the range of 0 and ${height - 1}" }

        return rawData[toArrayIndex(col, row)].color
    }

    operator fun get(col: Double, row: Double): Color {
        assert(col in .0..1.0) { "col must be in the range of 0.0 and 1.0" }
        assert(row in .0..1.0) { "row must be in the range of 0.0 and 1.0" }

        return rawData[toArrayIndex(col, row)].color
    }

    override fun toString() = "ImageBuffer(width=$width, height=$height, fallbackColor=$fallbackColor"

}

private fun Color.withGammaCorrection(gamma: Double): Color {
    assert(gamma >= 1.0) { "gamma must be greater or equal to 1.0" }
    val exponent = 1.0 / gamma
    return if (gamma == 1.0)
        this
    else
        color(
            r = r.pow(exponent),
            g = g.pow(exponent),
            b = b.pow(exponent)
        )

}

class ColorSample(
    private val fallbackColor: Color = Color.BLACK,
    private val minRelevantSamples: Int = DEFAULT_MIN_SAMPLES
) {

    private val colors: MutableList<Color> = mutableListOf()

    var variance: Double = 1.0
        private set

    val color: Color
        get() = when (colors.size) {
            0 -> fallbackColor
            1 -> colors.first()
            else -> colors.reduce { acc, color -> (acc + color) } / colors.size
        }

    fun push(color: Color) {
        colors.add(color)
        recalculateVariance()
    }

    private fun recalculateVariance() = if (colors.size < minRelevantSamples) {
        variance = 1.0
    } else {
        val luminance = colors.map { (r, g, b) -> (0.299 * r + 0.587 * g + 0.114 * b) }
        val luminanceMean = luminance.sum() / luminance.size
        variance = luminance.sumOf { lum -> (lum - luminanceMean) * (lum - luminanceMean) } / luminance.size
        logger.trace { "variance calculation: variance=$variance (colors=$colors)" }
    }

    companion object : KLogging() {
        private const val DEFAULT_MIN_SAMPLES = 2
    }

}

enum class ImageFormat {
    PPM,
    EXR,
    JPG
}
