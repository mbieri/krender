package scene

import math.P3d
import math.V3d
import math.Vector2d.Companion.uv
import math.Vector3d.Companion.vector
import math.unaryMinus
import mu.KLogging
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.assertThrows
import kotlin.test.assertEquals

private const val TEST_EPSILON = .001
private const val ASPECT_RATIO = 16.0 / 9.0
private const val FOCAL_LENGTH = 1.0

private const val LEFT_MOST = -1.0 * ASPECT_RATIO
private const val RIGHT_MOST = 1.0 * ASPECT_RATIO

internal class `PerspectiveCamera Tests` {

    private val camera = PerspectiveCamera(
        position = P3d.point(.0, .0, 1.0),
        direction = -V3d.Z,
        up = V3d.Y,
        aspectRatio = ASPECT_RATIO,
        focalLength = FOCAL_LENGTH
    )

    @Test
    fun `validate generated right vector`() {
        val (rightX, rightY, rightZ) = camera.right

        // compare vector components using a tolerance value
        assertEquals(RIGHT_MOST, rightX, TEST_EPSILON)
        assertEquals(.0, rightY, TEST_EPSILON)
        assertEquals(.0, rightZ, TEST_EPSILON)

    }

    @TestFactory
    fun `various camera rays`() =
        listOf(
            // "top" row
            uv(.0, .0) to vector(LEFT_MOST, -1.0, -FOCAL_LENGTH).normalized,
            uv(.5, .0) to vector(.0, -1.0, -FOCAL_LENGTH).normalized,
            uv(1.0, .0) to vector(RIGHT_MOST, -1.0, -FOCAL_LENGTH).normalized,
            // "center" row
            uv(.0, .5) to vector(LEFT_MOST, .0, -FOCAL_LENGTH).normalized,
            uv(.5, .5) to vector(.0, .0, -FOCAL_LENGTH).normalized,
            uv(1.0, .5) to vector(RIGHT_MOST, .0, -FOCAL_LENGTH).normalized,
            // "bottom" row
            uv(.0, 1.0) to vector(LEFT_MOST, 1.0, -FOCAL_LENGTH).normalized,
            uv(.5, 1.0) to vector(.0, 1.0, -FOCAL_LENGTH).normalized,
            uv(1.0, 1.0) to vector(RIGHT_MOST, 1.0, -FOCAL_LENGTH).normalized,
        ).map { (uv, direction) ->
            dynamicTest("map uv: $uv to camera ray direction: $direction") {
                val (_, d) = camera.generateCameraRay(uv)
                val (dx, dy, dz) = d

                assertEquals(direction.x, dx, TEST_EPSILON)
                assertEquals(direction.y, dy, TEST_EPSILON)
                assertEquals(direction.z, dz, TEST_EPSILON)
            }
        }

    @Test
    fun `failing asserts`() {
        assertThrows<AssertionError> {
            camera.generateCameraRay(uv(100.0, .0))
        }
    }

    companion object : KLogging()

}
