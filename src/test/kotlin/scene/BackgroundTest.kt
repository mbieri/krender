package scene

import math.Color
import math.UV
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.TestFactory
import kotlin.random.Random

internal class BackgroundTest {

    @Nested
    inner class SolidBackgroundTests {
        private val backgroundColor = Color.color(r = .15, g = .25, b = .85)
        private val background = Solid(backgroundColor)

        @TestFactory
        fun `random UV values create the same result`() = listOf(
            randomUV,
            randomUV,
            randomUV,
            randomUV,
            randomUV,
        ).map { uv ->
            dynamicTest("$uv -> $backgroundColor") {
                assertEquals(backgroundColor, background.backgroundColor(uv))
            }
        }

    }

    @Nested
    inner class VerticalGradientBackgroundTest {
        private val topColor = Color.color(r = .5, g = .7, b = 1.0)
        private val bottomColor = Color.WHITE
        private val gradientBG = VerticalGradient(topColor, bottomColor)

        @TestFactory
        fun `check gradient values`() = listOf(
            UV.uv(.5, 0.0) to bottomColor,
            UV.uv(.5, 0.5) to Color.color(r = .75, g = .85, b = 1.0),
            UV.uv(.5, 1.0) to topColor,
        ).map { (uv, gradientValue) ->
            dynamicTest("gradient value for $uv -> $gradientValue") {
                assertEquals(gradientValue, gradientBG.backgroundColor(uv))
            }
        }

    }

    companion object {

        val randomUV: UV
            get() = UV.uv(Random.nextDouble(), Random.nextDouble())

    }

}
