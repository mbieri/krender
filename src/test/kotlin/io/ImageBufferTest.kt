package io

import io.ImageFormat.PPM
import math.Vector3d.Companion.BLACK
import math.Vector3d.Companion.BLUE
import math.Vector3d.Companion.GREEN
import math.Vector3d.Companion.RED
import math.plus
import mu.KLogging
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.io.BufferedReader
import java.io.File
import java.io.FileReader

internal class `ImageBuffer Test` {

    @Nested
    inner class `instance creation and data representation` {

        @Test
        fun `instance creation`() {
            val imageBuffer = ImageBuffer(
                width = 16,
                height = 9,
                fallbackColor = BLUE
            )

            for (u in 0 until imageBuffer.width) {
                for (v in 0 until imageBuffer.height) {
                    assertEquals(BLUE, imageBuffer[u, v])
                }
            }

        }

        @Test
        fun `data access`() {
            val imageBuffer = ImageBuffer(
                width = 4,
                height = 4,
                fallbackColor = BLACK
            )

            val imageData = imageBuffer.data

            assertEquals(imageBuffer.width * imageBuffer.height, imageData.size)
            for (color in imageData) {
                assertEquals(BLACK, color)
            }

        }

        @Test
        fun `write and retrieve data`() {
            val imageBuffer = ImageBuffer(
                width = 4,
                height = 4,
                fallbackColor = BLACK
            )

            val bottomLeftColor = RED
            val bottomRightColor = GREEN
            val topLeftColor = bottomLeftColor + BLUE
            val topRightColor = bottomRightColor + BLUE

            imageBuffer[0, 0] = bottomLeftColor
            imageBuffer[imageBuffer.width - 1, 0] = bottomRightColor
            imageBuffer[0, imageBuffer.height - 1] = topLeftColor
            imageBuffer[imageBuffer.width - 1, imageBuffer.height - 1] = topRightColor

            assertEquals(bottomLeftColor, imageBuffer[0, 0])
            assertEquals(bottomRightColor, imageBuffer[imageBuffer.width - 1, 0])
            assertEquals(topLeftColor, imageBuffer[0, imageBuffer.height - 1])
            assertEquals(topRightColor, imageBuffer[imageBuffer.width - 1, imageBuffer.height - 1])
        }

        @Test
        fun `data array alignment`() {

            val imageBuffer = ImageBuffer(
                width = 4,
                height = 4,
                fallbackColor = BLACK
            )

            fun toArrayIndex(col: Int, row: Int) = imageBuffer.width * row + col

            val bottomLeftColor = RED
            val bottomRightColor = GREEN
            val topLeftColor = bottomLeftColor + BLUE
            val topRightColor = bottomRightColor + BLUE

            imageBuffer[0, 0] = bottomLeftColor
            imageBuffer[imageBuffer.width - 1, 0] = bottomRightColor
            imageBuffer[0, imageBuffer.height - 1] = topLeftColor
            imageBuffer[imageBuffer.width - 1, imageBuffer.height - 1] = topRightColor

            val imageData = imageBuffer.data

            assertEquals(imageBuffer.width * imageBuffer.height, imageData.size)

            // [bottomLeft....bottomRight....topLeft....topRight
            assertEquals(bottomLeftColor, imageData[toArrayIndex(0, 0)])
            assertEquals(bottomRightColor, imageData[toArrayIndex(imageBuffer.width - 1, 0)])
            assertEquals(topLeftColor, imageData[toArrayIndex(0, imageBuffer.height - 1)])
            assertEquals(topRightColor, imageData[toArrayIndex(imageBuffer.width - 1, imageBuffer.height - 1)])

        }

    }

    @Nested
    inner class `writing image files` {

        @Test
        fun `write image buffer to PPM file`() {
            val imageBuffer = ImageBuffer(
                width = 2,
                height = 2,
                fallbackColor = BLACK
            )

            val bottomLeftColor = RED
            val bottomRightColor = GREEN
            val topLeftColor = RED + BLUE
            val topRightColor = GREEN + BLUE

            imageBuffer[0, 0] = bottomLeftColor
            imageBuffer[imageBuffer.width - 1, 0] = bottomRightColor
            imageBuffer[0, imageBuffer.height - 1] = topLeftColor
            imageBuffer[imageBuffer.width - 1, imageBuffer.height - 1] = topRightColor

            val imageFile = File.createTempFile("testImage", "PPM")

            imageBuffer.writeToImage(imageFile, PPM)

            logger.debug { "image file written to ${imageFile.absolutePath}" }

            val expectedColorsInOrder = listOf(topLeftColor, topRightColor, bottomLeftColor, bottomRightColor)
                .map { color -> color.toRGB(255) }
                .map { (r, g, b) -> "$r $g $b" }

            imageFile.useLines {
                it.drop(3)
                    .forEachIndexed { index, line ->
                        assertEquals(expectedColorsInOrder[index], line, "wrong pixel order")
                    }
            }


        }

    }

    companion object : KLogging()

}