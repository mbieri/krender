package math

import math.Vector3d.Companion.ORIGIN
import math.Vector3d.Companion.X
import math.Vector3d.Companion.Y
import math.Vector3d.Companion.Z
import math.Vector3d.Companion.vector
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.TestFactory

internal class `Vector3d Tests` {

    @Nested
    inner class `Vector arithmetics` {

        @TestFactory
        fun `vector addition`() = listOf(
            vector(x = 1.0) to vector(x = 1.0) to vector(x = 2.0),
            vector(x = 1.0) to vector(x = 2.0) to vector(x = 3.0),
            vector(x = 1.0) to vector(y = 1.0) to vector(x = 1.0, y = 1.0),
            vector(x = 1.0) to vector(z = 1.0) to vector(x = 1.0, z = 1.0),
        ).map { (operands, result) ->
            dynamicTest("${operands.first} + ${operands.second} = $result") {
                assertEquals(result, operands.first + operands.second)
            }
        }

        @TestFactory
        fun `vector subtraction`() = listOf(
            vector(x = 1.0) to ORIGIN to vector(x = 1.0),
            vector(x = 1.0) to vector(x = -1.0) to vector(x = 2.0),
            vector(x = 1.0) to vector(y = 1.0) to vector(x = 1.0, y = -1.0),
        ).map { (operands, result) ->
            dynamicTest("${operands.first} - ${operands.second} = $result") {
                assertEquals(result, operands.first - operands.second)
            }
        }

        @TestFactory
        fun `dot product`() = listOf(
            Z to X to 0.0, // perpendicular -> 0
            X to X to 1.0, // collinear -> 1
            X to -X to -1.0 // opposite direction -> -1
        ).map { (operands, result) ->
            dynamicTest("${operands.first} . ${operands.second} = $result") {
                assertEquals(result, operands.first dot operands.second, EPSILON)
            }

        }

        @TestFactory
        fun `cross products`() = listOf(
            X to Y to Z,
            Y to Z to X,
            Z to X to Y
        ).map { (operands, result) ->
            dynamicTest("${operands.first} x ${operands.second} = $result") {
                assertEquals(result, operands.first cross operands.second)
            }
        }

    }

}
