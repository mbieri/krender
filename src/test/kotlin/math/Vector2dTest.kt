package math

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import kotlin.math.sqrt

class `Vector2d Test` {

    @Nested
    inner class `Data handling` {

        @Test
        fun copy() {
            val v1 = V2.vector(
                x = 1.0,
                y = 2.5
            )

            val newYValue = 3.5

            val v2 = v1.copy(
                y = newYValue
            )

            // v2.x is copied from v1.x
            assertEquals(v1.x, v2.x)
            // v2.y is equal to the set value of 3.5
            assertEquals(newYValue, v2.y)
        }

        @Test
        fun `length calculations`() {
            val v = V2.vector(1.0, 1.0)

            assertEquals(2.0, v.lengthSquared)
            assertEquals(sqrt(2.0), v.length)
        }

    }

    @Nested
    inner class `Data type handling` {

        @Test
        fun `vector as uv coordinates`() {
            val uv = V2.uv(.5, .25)

            assertEquals(.5, uv.u)
            assertEquals(.25, uv.v)
        }

    }

    @Nested
    inner class `Vector arithmetics` {

        @Test
        fun `negate a vector`() {
            val v = V2.vector(1.0, 2.5)

            val n = -v

            assertEquals(-v.x, n.x)
            assertEquals(-v.y, n.y)
        }

        @Test
        fun `adding two vectors`() {
            val v1 = V2.vector(1.0, 1.0)
            val v2 = V2.vector(2.0, 3.0)

            val sum = v1 + v2

            val (x, y) = sum

            assertEquals(3.0, x)
            assertEquals(4.0, y)

        }

        @Test
        fun `subtracting two vectors`() {
            val v1 = V2.vector(2.0, 2.0)
            val v2 = V2.vector(1.0, 1.0)
            val (sx, sy) = v1 - v2

            assertEquals(-1.0, sx)
            assertEquals(-1.0, sy)
        }

        @Test
        fun `multiply with scalar`() {
            val v = V2.vector(1.0, 2.0)
            val n = v * 2.0

            assertEquals(2.0, n.x)
            assertEquals(4.0, n.y)
        }

        @Test
        fun `dot product`() {
            val v1 = V2.vector(1.0, 1.0)
            val v2 = V2.vector(2.0, 2.0)

            val d = v1 dot v2

            assertEquals(4.0, d)
        }

    }

}
